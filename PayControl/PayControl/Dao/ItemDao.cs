﻿using PayControl.Models;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace PayControl.Dao
{
    public static class ItemDao
    {

        public static void CreateTable()
        {
            using (SQLiteConnection connection = new SQLiteConnection(DaoBase.dbConectString))
            {
                try
                {
                    connection.Open();

                    using (SQLiteCommand command = connection.CreateCommand())
                    {
                        string sqlText = "";
                        sqlText += "create table [Item](";
                        sqlText += "[item_id] integer primary key autoincrement";
                        sqlText += ", [item_created_time] datetime";
                        sqlText += ", [item_updated_time] datetime";
                        sqlText += ", [item_name] text";
                        sqlText += ", [price] integer";
                        sqlText += ", [tagString] text";
                        sqlText += ", [etc] text";
                        sqlText += ", [image_name] text";
                        sqlText += ", [delete_flg] integer";
                        sqlText += ")";
                        command.CommandText = sqlText;
                        command.ExecuteNonQuery();


                        sqlText = "";
                        sqlText += "create table [ItemToTag](";
                        sqlText += "[itt_id] integer primary key autoincrement";
                        sqlText += ", [itt_created_time] datetime";
                        sqlText += ", [itt_updated_time] datetime";
                        sqlText += ", [item_id] integer";
                        sqlText += ", [tag_id] integer";
                        sqlText += ", [delete_flg] integer";
                        sqlText += ")";
                        command.CommandText = sqlText;
                        command.ExecuteNonQuery();


                        sqlText = "";
                        sqlText += "create table [Tag](";
                        sqlText += "[tag_id] integer primary key autoincrement";
                        sqlText += ", [tag_created_time] datetime";
                        sqlText += ", [tag_updated_time] datetime";
                        sqlText += ", [tag_name] text";
                        sqlText += ", [delete_flg] integer";
                        sqlText += ")";
                        command.CommandText = sqlText;
                        command.ExecuteNonQuery();
                    }
                }
                catch (SQLiteException exception)
                {
                    MessageBox.Show(exception.ToString());
                }
            }
        }

        /// <summary>
        /// 抽出：アイテムの全レコードの読み込みを行う。
        /// 削除フラグが立っている物は除く。
        /// </summary>
        /// <returns></returns>
        public static List<ItemModel> AllItemRead()
        {
            // 結果格納用の変数
            List<ItemModel> results = new List<ItemModel>();
            
            // DBにアクセス
            using (SQLiteConnection connection = new SQLiteConnection(DaoBase.dbConectString))
            {
                try
                {
                    // 接続開始
                    connection.Open();

                    // SQLの実行環境の構築
                    using (SQLiteCommand command = connection.CreateCommand())
                    {
                        string sqlCommand = "";

                        // SQLコマンドの記述と設定
                        sqlCommand = "select * from Item outer left join ItemToTag on Item.item_id = ItemToTag.item_id where Item.delete_flg = 0";
                        command.CommandText = sqlCommand;

                        // SQLの読み取り環境の構築
                        using (SQLiteDataReader sqlReader = command.ExecuteReader())
                        {
                            // レコードのIDとして絶対に含まれることの無い-1を初期値として設定
                            int workingId = -1;

                            // レコード毎に読み出し
                            while (sqlReader.Read())
                            {
                                // タグで結合を行っている関係上連続して同じレコードを指している結果が返ってくるのでこれを対処
                                // 一つ前に読み込んでいるIDが現在読み込んでいるIDと被っている場合にタグのみを取得
                                if (int.Parse(sqlReader["item_id"].ToString()) == workingId)
                                {
                                    results[results.Count - 1].Tags.Add(TagModel.IdToObject(int.Parse(sqlReader["tag_id"].ToString())));
                                }

                                // IDが違っていればそれぞれの項目をキャストしながらモデルのパラメータにしてインスタンスを生成
                                else
                                {
                                    ItemModel model = new ItemModel(
                                        int.Parse(sqlReader["item_id"].ToString()),
                                        Builder.DateTimeBuilder(sqlReader["item_created_time"].ToString()),
                                        Builder.DateTimeBuilder(sqlReader["item_updated_time"].ToString()),
                                        sqlReader["item_name"].ToString(),
                                        int.Parse(sqlReader["price"].ToString()),
                                        sqlReader["etc"].ToString(),
                                        sqlReader["image_name"].ToString(),
                                        int.Parse(sqlReader["delete_flg"].ToString())
                                        );

                                    // 結果として追加
                                    results.Add(model);

                                    // タグがアイテムに設定されていることを確認してモデルにタグを追加
                                    if (sqlReader["tag_id"].ToString() != "")
                                    {
                                        results[results.Count - 1].Tags.Add(TagModel.IdToObject(int.Parse(sqlReader["tag_id"].ToString())));
                                    }

                                    // 現在操作しているアイテムのIDを次のループで利用するため一時保存
                                    workingId = int.Parse(sqlReader["item_id"].ToString());
                                }
                            }
                        }
                    }
                }

                // SQLでのエラーに対処：メッセージボックスでエラー内容を表示
                catch (SQLiteException e)
                {
                    MessageBox.Show(e.ToString());
                }
            }

            // 読み取り結果を返却
            return results;
        }
        
        /// <summary>
        /// 引数で渡されたModelを基にDBに登録
        /// </summary>
        /// <param name="model"></param>
        public static void Insert(ItemModel model)
        {
            model.Tags = TagDao.CheckedInsert(model.Tags);

            // DBにアクセス
            using (SQLiteConnection connection = new SQLiteConnection(DaoBase.dbConectString))
            {
                try
                {
                    // 接続開始
                    connection.Open();

                    // SQLの実行環境の構築
                    using (SQLiteCommand command = connection.CreateCommand())
                    {
                        string sqlText = "";

                        // SQLコマンドの記述と設定
                        sqlText += "insert into Item(item_created_time, item_updated_time,item_name, price, etc, image_name, delete_flg)";
                        sqlText += string.Format(
                            "values({0},{1},'{2}',{3},'{4}','{5}', {6})",
                            "datetime('now', 'localtime')",
                            "datetime('now', 'localtime')", 
                            model.Name, model.Price, model.Etc,model.ImageName, 0
                            );
                        command.CommandText = sqlText;

                        // SQLの実行
                        command.ExecuteNonQuery();


                        
                        sqlText = "select * from Item order by item_id desc limit 1";

                        command.CommandText = sqlText;
                        // SQLの読み取り環境の構築
                        using (SQLiteDataReader sqlReader = command.ExecuteReader())
                        {
                            // レコード毎に読み出し
                            sqlReader.Read();

                            model.ID = int.Parse(sqlReader["item_id"].ToString());
                        }
                    }
                }

                // SQLでのエラーに対処：メッセージボックスでエラー内容を表示
                catch (SQLiteException exception)
                {
                    MessageBox.Show(exception.ToString());
                }
            }
            
            ItemToTagDao.Insert(model);
            
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="model"></param>
        public static void Update(ItemModel model)
        {
            // タグの書き換えが面倒くさいので一旦関連タグを削除
            ItemToTagDao.DeleteItemID(model.ID);

            // modelに登録されているタグが登録されているタグか判別して再度代入
            model.Tags = TagDao.CheckedInsert(model.Tags);

            // タグのIDが分かったところでタグの関連付けを登録
            ItemToTagDao.Insert(model);



            // DBにアクセス
            using (SQLiteConnection connection = new SQLiteConnection(DaoBase.dbConectString))
            {
                try
                {
                    // 接続開始
                    connection.Open();

                    // SQLの実行環境の構築
                    using (SQLiteCommand command = connection.CreateCommand())
                    {
                        // SQLコマンドの記述と設定
                        string sqlText = "";
                        sqlText += string.Format("update Item set item_updated_time = datetime('now', 'localtime'),item_name='{0}', price='{1}', etc='{2}', image_name='{3}', delete_flg={4}",
                            model.Name, model.Price.ToString(), model.Etc, model.ImageName, model.DeleteFlag);
                        sqlText += " where item_id='" + model.ID+"'";
                        command.CommandText = sqlText;
                        command.ExecuteNonQuery();
                    }
                }

                // SQLでのエラーに対処：メッセージボックスでエラー内容を表示
                catch (SQLiteException exception)
                {
                    MessageBox.Show(exception.ToString());
                }
            }
        }

    }
}
