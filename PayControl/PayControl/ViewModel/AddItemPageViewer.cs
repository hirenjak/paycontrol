﻿using PayControl.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayControl.ViewModel
{
    public class AddItemPageViewer : BaseViewModel
    {
        public ItemModel ItemModel { get; set; }

        private string imageName;
        private string imagePath;

        /// <summary>
        /// imageNameがセットされた時点でimagePathをセットするようにする。
        /// </summary>
        public string ImageName
        {
            get
            {
                return imageName;
            }
            set
            {
                imageName = value;
                ImagePath = value;
            }
        }

        /// <summary>
        /// imagePathは絶対アドレスに変換する。
        /// </summary>
        public string ImagePath
        {
            get
            {
                return imagePath;
            }
            private set
            {
                imagePath = Path.GetFullPath(Properties.Settings.Default.TempPath + value);
            }
        }
        public AddItemPageViewer()
        {
            ItemModel = new ItemModel("",0,"");
        }
        
    }
}
