﻿using PayControl.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayControl.ViewModel
{
    class DetailPageViewer : BaseViewModel
    {
        public ItemModel ItemModel { get; set; }

        public DetailPageViewer()
        {
            ItemModel = new ItemModel("", 0, "");
        }
        
    }
}
