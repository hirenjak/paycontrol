﻿using PayControl.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayControl.ViewModel
{
    public class EditPageViewer : BaseViewModel
    {
        private string imageName;
        private string imagePath;

        public ItemModel ItemModel { get; set; }

        /// <summary>
        /// imageNameがセットされた時点でimagePathをセットするようにする。
        /// </summary>
        public string ImageName
        {
            get
            {
                return imageName;
            }
            set
            {
                imageName = value;
                ImagePath = value;
            }
        }

        /// <summary>
        /// imagePathは絶対アドレスに変換する。
        /// </summary>
        public string ImagePath
        {
            get
            {
                return imagePath;
            }
            private set
            {
                imagePath = Path.GetFullPath(Properties.Settings.Default.TempPath + value);
            }
        }


        public EditPageViewer()
        {
            ItemModel = new ItemModel("", 0, "");
            ImageName = "";
        }

        public void ImagePathSet()
        {
            ItemModel.ImageName = ImageName;
        }
        
    }
}
