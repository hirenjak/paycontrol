﻿using PayControl.Pages;
using PayControl.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PayControl.Dao;
using PayControl.ViewModel;

namespace PayControl
{

    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        public NavigationService nService;

        public List<TagModel> tagLists;

        MainWindowViewer viewer;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            int temp = 52;
            this.Width = 9 * temp;
            this.Height = 16 * temp;

            viewer = new MainWindowViewer();

            // xamlで生成したNavigationServiceを取得
            nService = myFrame.NavigationService;
            
            // ファイルおよびフォルダの作成
            if (!File.Exists(Properties.Settings.Default.DBPath))
            {
                ItemDao.CreateTable();
            }

            if (!File.Exists(Properties.Settings.Default.ResourcesPath))
            {
                Directory.CreateDirectory(Properties.Settings.Default.ResourcesPath);
            }

            if (!File.Exists(Properties.Settings.Default.TempPath))
            {
                Directory.CreateDirectory(Properties.Settings.Default.TempPath);
            }
            else
            {
                DirectoryInfo dInfo = new DirectoryInfo(Properties.Settings.Default.TempPath);
                foreach(var value in dInfo.GetFiles())
                {
                    value.Delete();
                }
            }

            // 初期化のためのページ遷移
            ToMainPage();
        }
        
        public void Refresh()
        {
            listView.DataContext = viewer.ItemModels;

            listView.Items.Refresh();
        }

        ItemModel targetItemModel;


        public void ListView_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ListViewItem targetItem = (ListViewItem)sender;

            targetItemModel = (ItemModel)targetItem.DataContext;

            nService.Navigate(new DetailPage(this, targetItemModel));
        }

        /// <summary>
        /// メインページに遷移する。
        /// 描画にかかわるものを再読み込みする。
        /// </summary>
        public void ToMainPage()
        {
            TagModel.ReloadTagLists();
            viewer.ItemModels = new ObservableCollection<ItemModel>(ItemDao.AllItemRead());
            Refresh();

            nService.Navigate(new MainPage(this));
        }

        public void ToAddItemPage()
        {
            nService.Navigate(new AddItemPage(this));
        }

        public void ToEditPage()
        {
            nService.Navigate(new EditPage(this,targetItemModel));
        }
    }
}
