﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayControl.Dao
{
    public static class DaoBase
    {
        public static string dbPath { get; }
        public static string dbConectString { get; }

        static DaoBase()
        {
            dbPath = Properties.Settings.Default.DBPath;
            dbConectString = "Data Source=" + dbPath;
        }
    }
}
