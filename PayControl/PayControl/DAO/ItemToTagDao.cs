﻿using PayControl.Models;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace PayControl.Dao
{
    class ItemToTagDao
    {

        /// <summary>
        /// 引数で渡されたModelを基にDBに登録
        /// </summary>
        /// <param name="model"></param>
        public static void Insert(ItemModel model)
        {
            // DBにアクセス
            using (SQLiteConnection connection = new SQLiteConnection(DaoBase.dbConectString))
            {
                try
                {
                    // 接続開始
                    connection.Open();

                    // SQLの実行環境の構築
                    using (SQLiteCommand command = connection.CreateCommand())
                    {
                        foreach (var value in model.Tags)
                        {
                            string sqlText = "";

                            // SQLコマンドの記述と設定
                            sqlText += "insert into ItemToTag(itt_created_time, itt_updated_time,item_id, tag_id, delete_flg)";
                            sqlText += string.Format(
                                "values({0},{1},{2},{3},{4})",
                                "datetime('now', 'localtime')",
                                "datetime('now', 'localtime')",
                                model.ID, value.ID, 0
                                );
                            command.CommandText = sqlText;

                            // SQLの実行
                            command.ExecuteNonQuery();
                        }
                    }
                }

                // SQLでのエラーに対処：メッセージボックスでエラー内容を表示
                catch (SQLiteException exception)
                {
                    MessageBox.Show(exception.ToString());
                }
            }
        }

        public static void DeleteItemID(int ItemId)
        {
            // DBにアクセス
            using (SQLiteConnection connection = new SQLiteConnection(DaoBase.dbConectString))
            {
                try
                {
                    // 接続開始
                    connection.Open();

                    // SQLの実行環境の構築
                    using (SQLiteCommand command = connection.CreateCommand())
                    {
                            string sqlText = "";

                            // SQLコマンドの記述と設定
                            sqlText += "delete from ItemToTag where item_id = '" + ItemId + "'";

                            command.CommandText = sqlText;

                            // SQLの実行
                            command.ExecuteNonQuery();
                        }
                    
                }

                // SQLでのエラーに対処：メッセージボックスでエラー内容を表示
                catch (SQLiteException exception)
                {
                    MessageBox.Show(exception.ToString());
                }
            }
        }
    }
}
