﻿using PayControl.Models;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace PayControl.Dao
{
    public class TagDao
    {
        /// <summary>
        /// 抽出：タグの全レコードの読み込みを行う。
        /// </summary>
        /// <returns></returns>
        public static List<TagModel> AllRead()
        {
            List<TagModel> results = new List<TagModel>();

            // DBにアクセス
            using (SQLiteConnection connection = new SQLiteConnection(DaoBase.dbConectString))
            {
                try
                {
                    connection.Open();

                    using (SQLiteCommand command = connection.CreateCommand())
                    {
                        String sqlCommand = "";

                        // SQLコマンドの記述と設定
                        sqlCommand = "select * from Tag";
                        command.CommandText = sqlCommand;

                        using (SQLiteDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                TagModel tag = new TagModel(
                                    int.Parse(sqlReader["tag_id"].ToString()),
                                    Builder.DateTimeBuilder(sqlReader["tag_created_time"].ToString()),
                                    Builder.DateTimeBuilder(sqlReader["tag_updated_time"].ToString()),
                                    sqlReader["tag_name"].ToString(),
                                    int.Parse(sqlReader["delete_flg"].ToString())
                                    );

                                results.Add(tag);
                            }
                        }
                    }
                }

                // SQLでのエラーに対処：メッセージボックスでエラー内容を表示
                catch (SQLiteException e)
                {
                    MessageBox.Show(e.ToString());
                }
            }

            return results;
        }

        public static TagModel Read(string targetName)
        {
            List<TagModel> results = new List<TagModel>();

            // DBにアクセス
            using (SQLiteConnection connection = new SQLiteConnection(DaoBase.dbConectString))
            {
                try
                {
                    connection.Open();

                    using (SQLiteCommand command = connection.CreateCommand())
                    {
                        String sqlCommand = "";

                        // SQLコマンドの記述と設定
                        sqlCommand = "select * from Tag where tag_name = '" + targetName + "'";
                        command.CommandText = sqlCommand;

                        using (SQLiteDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                TagModel tag = new TagModel(
                                    int.Parse(sqlReader["tag_id"].ToString()),
                                    Builder.DateTimeBuilder(sqlReader["tag_created_time"].ToString()),
                                    Builder.DateTimeBuilder(sqlReader["tag_updated_time"].ToString()),
                                    sqlReader["tag_name"].ToString(),
                                    int.Parse(sqlReader["delete_flg"].ToString())
                                    );

                                results.Add(tag);
                            }
                        }
                    }
                }

                // SQLでのエラーに対処：メッセージボックスでエラー内容を表示
                catch (SQLiteException e)
                {
                    MessageBox.Show(e.ToString());
                }
            }

            return results[0];
        }



        /// <summary>
        /// 引数で渡されたModelを基にDBに登録
        /// </summary>
        /// <param name="model"></param>
        private static TagModel Insert(TagModel model)
        {
            // DBにアクセス
            using (SQLiteConnection connection = new SQLiteConnection(DaoBase.dbConectString))
            {
                try
                {
                    // 接続開始
                    connection.Open();

                    // SQLの実行環境の構築
                    using (SQLiteCommand command = connection.CreateCommand())
                    {
                        string sqlText = "";

                        // SQLコマンドの記述と設定
                        sqlText += "insert into Tag(tag_created_time, tag_updated_time,tag_name, delete_flg)";
                        sqlText += string.Format(
                            "values({0},{1},'{2}',{3})",
                            "datetime('now', 'localtime')",
                            "datetime('now', 'localtime')",
                            model.Name, 0
                            );
                        command.CommandText = sqlText;

                        // SQLの実行
                        command.ExecuteNonQuery();
                    }
                }

                // SQLでのエラーに対処：メッセージボックスでエラー内容を表示
                catch (SQLiteException exception)
                {
                    MessageBox.Show(exception.ToString());
                }
            }

            return Read(model.Name);
        }
        public static TagModel CheckedInsert(TagModel model)
        {
            if(model.ID == -1)
            {
                model = Insert(model);
            }

            return model;
        }

        public static List<TagModel> CheckedInsert(List<TagModel> models)
        {
            for (int id = 0; id < models.Count; id++)
            {
                models[id] = CheckedInsert(models[id]);
            }

            return models;
        }
    }
}
