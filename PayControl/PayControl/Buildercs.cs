﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayControl
{
    public static class Builder
    {
        public static Uri UriBuilder(string target)
        {
            try
            {
                return new Uri(target);
            }
            catch
            {
                return new Uri("https://www.google.co.jp");
            }
        }

        public static DateTime DateTimeBuilder(string target)
        {
            try
            {
                return DateTime.Parse(target);
            }
            catch
            {
                return DateTime.Now;
            }
        }
    }
}
