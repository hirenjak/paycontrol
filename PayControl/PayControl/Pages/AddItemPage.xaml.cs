﻿using Microsoft.Win32;
using PayControl.Dao;
using PayControl.Models;
using PayControl.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PayControl.Pages
{
    /// <summary>
    /// AddItemPage.xaml の相互作用ロジック
    /// </summary>
    public partial class AddItemPage : Page
    {
        MainWindow mainWindow;
        AddItemPageViewer viewer;


        public AddItemPage(MainWindow mainWindow)
        {
            InitializeComponent();

            // タグ用テキストボックスに利用する変数の初期化
            textBoxes = new List<TextBox>();
            stackPanels = new List<StackPanel>();

            this.mainWindow = mainWindow;

            viewer = new AddItemPageViewer();
            this.DataContext = viewer;
        }

        public void BackButton_Click(object sender, RoutedEventArgs e)
        {
            mainWindow.ToMainPage();
        }

        
        // タグ用テキストボックスに利用する変数
        List<TextBox> textBoxes;
        List<StackPanel> stackPanels;

        /// <summary>
        /// タグ用のテキストボックス追加ボタン
        /// 5個ずつ並べて表示されるように処理する。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void TagAddkButton_Click(object sender, RoutedEventArgs e)
        {
            TagTextBoxAdd("");
        }

        public void ApplyButton_Click(object sender, RoutedEventArgs e)
        {
            foreach (var value in textBoxes)
            {
                viewer.ItemModel.Tags.Add(TagModel.NameToObject(value.Text));
            }

            viewer.ItemModel.ImageName = viewer.ImageName;
            File.Move(viewer.ImagePath, viewer.ItemModel.ImagePath);

            ItemDao.Insert(viewer.ItemModel);

            mainWindow.ToMainPage();
        }

        private void TagTextBoxAdd(string text)
        {
            // テキストボックスが5個1セットとなるよ鵜にするために現在個数で判断
            if (textBoxes.Count % 5 == 0)
            {
                // スタックパネルを追加
                stackPanels.Add(new StackPanel()
                {
                    Orientation = Orientation.Horizontal
                });

                tagStackPanel.Children.Add(stackPanels[stackPanels.Count - 1]);
            }


            // テキストボックスを追加
            textBoxes.Add(new TextBox()
            {
                Text = text,
                Height = 24,
                Width = 48,
                Margin = new Thickness(4, 4, 0, 0),
                Style = (Style)FindResource("myTextBox")
            });

            // テキストボックスを追加されたスタックパネルに追加
            stackPanels[stackPanels.Count - 1].Children.Add(textBoxes[textBoxes.Count - 1]);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ImageButton_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new OpenFileDialog()
            {
                Title = "ファイルを開く",
                Filter = "Image Files(*.jpg,*.bmp,*.png)|*.jpg;*.bmp;*.png"
            };

            if (dialog.ShowDialog() == true)
            {
                string targetFileName = DateTime.Now.Ticks.ToString();
                File.Copy(dialog.FileName, Properties.Settings.Default.TempPath + targetFileName);

                viewer.ImageName = targetFileName;
            }
            else
            {
            }
        }
    }
}
