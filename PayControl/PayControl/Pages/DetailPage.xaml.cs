﻿using PayControl.Models;
using PayControl.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PayControl.Pages
{
    /// <summary>
    /// DetailPage.xaml の相互作用ロジック
    /// </summary>
    public partial class DetailPage : Page
    {
        MainWindow mainWindow;
        DetailPageViewer viewer;

        public DetailPage(MainWindow mainWindow, ItemModel targetModel)
        {
            InitializeComponent();

            this.mainWindow = mainWindow;

            viewer = new DetailPageViewer();
            viewer.ItemModel = targetModel;
            this.DataContext = viewer;

        }

        private void BackButton_Click_1(object sender, RoutedEventArgs e)
        {
            mainWindow.ToMainPage();
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            mainWindow.ToEditPage();
        }
    }
}
