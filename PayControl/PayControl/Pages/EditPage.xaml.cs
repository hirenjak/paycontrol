﻿using Microsoft.Win32;
using PayControl.Dao;
using PayControl.Models;
using PayControl.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PayControl.Pages
{
    /// <summary>
    /// EditPage.xaml の相互作用ロジック
    /// </summary>
    public partial class EditPage : Page
    {
        // 遷移用のメソッドを使用するのに使用
        MainWindow mainWindow;

        // タグ用テキストボックスに利用する変数
        List<TextBox> textBoxes;
        List<StackPanel> stackPanels;

        EditPageViewer viewer;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="mainWindow"></param>
        /// <param name="targetModel"></param>
        public EditPage(MainWindow mainWindow, ItemModel targetModel)
        {
            // コンポーネントの初期化
            InitializeComponent();

            // タグ用テキストボックスに利用する変数の初期化
            textBoxes = new List<TextBox>();
            stackPanels = new List<StackPanel>();

            // 外部からのデータを取得
            this.mainWindow = mainWindow;

            viewer = new EditPageViewer()
            {
                ItemModel = targetModel
            };
            this.DataContext = viewer;

            
            foreach(var value in viewer.ItemModel.Tags)
            {
                TagTextBoxAdd(value.Name);
            }
        }

        /// <summary>
        /// 戻るボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void BackButton_Click(object sender, RoutedEventArgs e)
        {
            // 特に何もせずにメイン画面に遷移
            mainWindow.ToMainPage();
        }

        /// <summary>
        /// 確認ボタン
        /// ItemDaoを経由してDBにアクセスする。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ApplyButton_Click(object sender, RoutedEventArgs e)
        {
            // 変更箇所を対象モデルに代入

            viewer.ItemModel.Tags = new List<TagModel>();

            foreach (var value in textBoxes)
            {
                viewer.ItemModel.Tags.Add(TagModel.NameToObject(value.Text));
            }

            if (viewer.ImageName != "")
            {
                viewer.ItemModel.ImageName = viewer.ImageName;
                File.Move(viewer.ImagePath, viewer.ItemModel.ImagePath);
            }
            // 変更用メソッドでDBを更新
            ItemDao.Update(viewer.ItemModel);

            // メイン画面に遷移
            mainWindow.ToMainPage();
        }

        /// <summary>
        /// タグ用のテキストボックス追加ボタン
        /// 5個ずつ並べて表示されるように処理する。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void TagAddkButton_Click(object sender, RoutedEventArgs e)
        {
            TagTextBoxAdd("");
        }

        private void TagTextBoxAdd(string text)
        {
            // テキストボックスが5個1セットとなるよ鵜にするために現在個数で判断
            if (textBoxes.Count % 5 == 0)
            {
                // スタックパネルを追加
                stackPanels.Add(new StackPanel()
                {
                    Orientation = Orientation.Horizontal
                });

                tagStackPanel.Children.Add(stackPanels[stackPanels.Count - 1]);
            }


            // テキストボックスを追加
            textBoxes.Add(new TextBox()
            {
                Text = text,
                Height = 24,
                Width = 48,
                Margin = new Thickness(4, 4, 0, 0),
                Style = (Style)FindResource("myTextBox")
            });

            // テキストボックスを追加されたスタックパネルに追加
            stackPanels[stackPanels.Count - 1].Children.Add(textBoxes[textBoxes.Count - 1]);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ImageButton_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new OpenFileDialog()
            {
                Title = "ファイルを開く",
                Filter = "Image Files(*.jpg,*.bmp,*.png)|*.jpg;*.bmp;*.png"
            };

            if (dialog.ShowDialog() == true)
            {
                string targetFileName = DateTime.Now.Ticks.ToString();
                File.Copy(dialog.FileName, Properties.Settings.Default.TempPath + targetFileName);

                viewer.ImageName = targetFileName;
            }
            else
            {
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            viewer.ItemModel.DeleteFlag = 1;
            ItemDao.Update(viewer.ItemModel);

            mainWindow.ToMainPage();
        }
    }
}
