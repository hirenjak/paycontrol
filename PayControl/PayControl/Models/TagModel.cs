﻿using PayControl.Dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayControl.Models
{
    public class TagModel:BaseModel
    {
        public static List<TagModel> TagLists { get; private set; }

        static TagModel()
        {
            TagLists = TagDao.AllRead();
        }
        
        public string Name { get; set; }



        public static void ReloadTagLists()
        {
            TagLists = TagDao.AllRead();
        }

        /// <summary>
        /// タグ名からタグのリスト中に存在するかを確認する。
        /// 存在すればそのTagオブジェクトを、無ければnullを返す。
        /// </summary>
        /// <param name="targetName">対象となるタグ名</param>
        /// <returns></returns>
        public static TagModel NameToObject(string targetName)
        {
            foreach (var value in TagLists)
            {
                if (value.Name == targetName) { return value; }
            }
            return new TagModel(-1,DateTime.Now, DateTime.Now,targetName, 0);
        }

        public static TagModel IdToObject(int targetId)
        {
            foreach (var value in TagLists)
            {
                if (value.ID == targetId) { return value; }
            }
            return null;
        }

        public TagModel(int ID, DateTime CreateTime, DateTime UpdateTime, string Name, int DeleteFlag) : base(ID, CreateTime, UpdateTime, DeleteFlag)
        {
            this.Name = Name;
        }

        public static List<TagModel> StringToTagList(string targetString, char splitChar)
        {
            List<TagModel> resultLists = new List<TagModel>();

            string[] targetStrings = targetString.Split(splitChar);

            foreach(var value in targetStrings)
            {
                resultLists.Add(NameToObject(value));
            }

            return resultLists;
        }
    }
}
