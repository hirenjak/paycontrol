﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayControl.Models
{
    class ItemToTagModel :BaseModel
    {
        public int ItemID { get; set; }
        public int TagID { get; set; }

        public ItemToTagModel(int ID, DateTime CreateTime, DateTime UpdateTime, int DeleteFlag) : base(ID, CreateTime, UpdateTime, DeleteFlag)
        {

        }
        
    }
}
