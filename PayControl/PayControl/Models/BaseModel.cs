﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayControl.Models
{
    public abstract class BaseModel
    {
        public int ID { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime UpdateTime { get; set; }
        public int DeleteFlag { get; set; }

        public BaseModel(int ID, DateTime CreateTime, DateTime UpdateTime, int DeleteFlag)
        {
            this.ID = ID;
            this.CreateTime = CreateTime;
            this.UpdateTime = UpdateTime;
            this.DeleteFlag = DeleteFlag;
        }
    }
}
