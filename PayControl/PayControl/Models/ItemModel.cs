﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace PayControl.Models
{
    public class ItemModel : BaseModel
    {
        private string imageName;
        private string imagePath;

        public string Name { get; set; }
        public int Price { get; set; }
        public List<TagModel> Tags { get; set; }
        public string Etc { get; set; }

        /// <summary>
        /// imageNameがセットされた時点でimagePathをセットするようにする。
        /// </summary>
        public string ImageName { get
            {
                return imageName;
            }
            set
            {
                imageName = value;
                ImagePath = value;
            }
        }

        /// <summary>
        /// imagePathは絶対アドレスに変換する。
        /// </summary>
        public string ImagePath {
            get
            {
                return imagePath;
            }
        private set
            {
                imagePath = Path.GetFullPath(Properties.Settings.Default.ResourcesPath + value);
            }
        }

        public string TagString { get
            {
                string tagSting = "";

                foreach (var value in Tags)
                {
                    if (value != null)
                    {
                        tagSting += value.Name + "-";
                    }
                }

                return tagSting;
            }
        }
        
        public ItemModel(int ID, DateTime CreateTime, DateTime UpdateTime, string Name, int Price, string Etc, string ImageName, int DeleteFlag) : base(ID, CreateTime, UpdateTime, DeleteFlag)
        {
            this.Name = Name;
            this.Price = Price;
            this.Etc = Etc;
            this.ImageName = ImageName;

            Tags = new List<TagModel>();
            
        }

        public ItemModel(string Name, int Price, string Etc) : this(-1, DateTime.Now, DateTime.Now, Name, Price, Etc, @"Default.jpg", -1) { }

        public ItemModel(string Name, int Price, string Etc, string ImagePath) : this(-1, DateTime.Now, DateTime.Now, Name, Price, Etc, ImagePath, -1) { }

        public string TagToString()
        {
            string resultString = "";

            foreach(var value in Tags)
            {
                resultString += value + ",";
            }

            return resultString;
        }

    }
}
